
Contour

Android drawing teacher application

(C) Copyright 2014 by Johannes Riedmann (e0926649@student.tuwien.ac.at)
_____________________________________________________________________________________________________

The following steps are required to develop, run and deploy Contour.

These instructions are applicable for Windows 7 (64 bit) systems.

For other operating systems, adapt comparable mechanisms.

_____________________________________________________________________________________________________

1 Software needed to develop/run Contour on your device:

-	Git
	Download a git client, e.g. http://git-scm.com/download/win

-	Java
	Download the latest JDK from http://www.oracle.com/technetwork/java/javase/downloads/index.html

-	Eclipse ADT
	Download the Eclipse Android Developer Tools from http://developer.android.com/sdk/index.html

-	Project sources
	Pull the repository from https://j0h1@bitbucket.org/j0h1/contour-for-android.git
	
_____________________________________________________________________________________________________

2 How to install Contour from apk (Android applicaton package)

Download a File Manager from Google Play Store, e.g. Astro File Manager, copy Contour.apk, located
at the base folder of the project onto your device and run it, using the File Manager. The app
will be installed on your device.

NOTE: The pre-built APK for Contour uses a already deployed RESTful Web Service. If you want to test
the web service by yourself, you need to follow the instructions below.

_____________________________________________________________________________________________________

3 How to get things started as developer

3.1 Development: Contour (Android application)

3.1.1
Open Eclipse (don't select the "contour_for_android" folder or any folder of this hierarchy as Workspace) 
-> File -> Import... -> Android -> Existing Android Code Into Workspace 
-> Select the "Contour" folder (contains Android project) as root directory 
-> If two projects show up, only select the project with "New Project Name": "Contour"

3.1.2 
You can now run the project by selecting the project folder in the Package Explorer and use
-> Run -> Run As -> Android Application
-> When you want to use your own device, you need to install the appropriate USB driver (that the ADB can
connect to it) and enable USB Debugging in the Developer Settings on your device
-> Otherwise specify a new Virtual Device


3.2 Development: contour_server (RESTful Web Service)

3.2.1
-> File -> Import... -> General -> Existing Projects into Workspace
-> Select the "contour_server" (contains RESTful Web Service) folder as root directory -> finish

3.2.2
Eventual bugs:
-> "Syntax error, annotations are only available if source level is 1.5 or higher" -> Right click on project in
Package Explorer -> Properties -> Java Compiler -> check "Enable project specific settings -> select JDK
Compliance to 1.7 -> Apply -> OK

3.2.3
If you want to run the HTTP Server locally:
-> Start the HSQL database by executing "contourDatabase.bat", located in the "contour_database\bin" folder
-> Right click on "RestServer.java" in the package "com.j0h1.contour.server" -> Run As -> Java Application
-> If you want to access the Web Service from the Android application, you need to forward port 8080 on 
your router (and eventually disable the firewall)
-> As is, the Android application is configured to use the already deployed Web Service, but the IP of the
Web Server used by the Android Application can be changed by setting the REST_SERVICE_IP constant in the
RESTConfig class, located in the package "com.j0h1.contour.utils" (localhost won't do the trick, since the
application runs on a different device, specify the actual public IP address of the host computer)


3.3 Development: contour_admin (GWT Web application)

3.3.1
Download the GWT Plugin for Eclipse by using the appropriate update site for your Eclipse version from here:
https://developers.google.com/eclipse/docs/getting_started?hl=de
-> Start Eclipse -> Help -> Install New Software... -> Add... -> Specify a name, e.g. "GWT Plugin for Eclipse" and
the update site as location, e.g. https://dl.google.com/eclipse/plugin/4.2 for Eclipse Juno (current version in ADT)
-> Select "Google Plugin for Eclipse (required)" and under "SDK" the "Google Web Toolkit SDK 2.6.0" -> install

3.3.2
-> File -> Import... -> General -> Existing Projects into Workspace
-> Select the "contour_admin" (contains GWT Web Application) folder as root directory -> finish

3.3.3
Eventual bugs:
-> "Syntax error, annotations are only available if source level is 1.5 or higher"
		Solution:
		Right click on project in Package Explorer -> Properties -> Java Compiler 
		check "Enable project specific settings -> select JDK Compliance to 1.7 -> Apply -> OK

-> No GWT SDK is found in the class path
		Solution:
		Right click on Project in Package Explorer -> Google -> Web Toolkit Settings...
		-> uncheck "Use Google Web Toolkit" -> press OK -> check it again -> press OK

-> The application was developed with the GWT SDK 2.5.1, since the GWT Designer was only available for this
specific version. This leads to a problem with the "gwt-servlet.jar":
"The file war\WEB-INF\lib\gwt-servlet.jar has a different size than GWT SDK library gwt-servlet.jar; 
perhaps it is a different version?"
		Solution:
		-> Navigate to the GWT Plugin folder: 
		"[ADT Bundle location]\adt-bundle-windows-x86_64-20140702\eclipse\plugins\com.google.gwt.eclipse.sdkbundle_2.6.0\gwt-2.6.0"
		-> copy the "gwt-servlet.jar" into the project's "war/WEB-INF/lib" folder and replace it

3.3.4
Start the HSQL database by executing "contourDatabase.bat", located in the "contour_database\bin" folder (if not already started)

3.3.5
To run the application: (you need to use Google Chrome to run the GWT Application in Development mode)
-> Select project folder in the Package Explorer -> Run As -> Web Application -> Wait a few seconds
-> Click on the generated URL under the tab "Development Mode" -> when launching the application for the first time,
you get prompted to install the GWT Developer Plugin for Chrome -> follow the instructions and install it
-> after the installation is finished, refresh the link -> this could take a while, because the development server
has to be started
-> Login data:
	Username: ContourAdmin
	Password: j0h1PW2014

_____________________________________________________________________________________________________

4 How to deploy the RESTful Web Service and the GWT Web application

4.1 Deployment: contour_server (RESTful Web Service)

4.1.1
-> Right click on project folder in Package Explorer -> Export... -> Java -> Runnable Java Archive
-> Select previously used Launch configuration, choose Export destination and "Extract required libraries
into generated JAR" as Library handling -> Done

4.2 Deployment: contour_admin (GWT Web application)

4.2.1
To deploy the GWT application locally, a preconfigured Apache tomcat server is included in the repository.
The Tomcat server needs some environment variables set to work:
-> JAVA_HOME should be pointing to the JDK folder, e.g. C:\Program Files\Java\jdk1.8.0_20
-> CATALINA_HOME should be pointing to the apache tomcat folder, e.g. C:\apache-tomcat-8.0.12
-> JAVA_OPTS should have the following value: "-Djava.library.path=%CATALINA_HOME%\shared\lib" (including the quotation marks)
-> LD_LIBRARY_PATH should point to the Apache Tomcat's "shared\lib" folder, e.g. %CATALINA_HOME%\shared\lib
-> then add %CATALINA_HOME%\bin to the PATH variable

4.2.2
Now you can run the "startup.bat" file in Tomcat's "bin" folder. After the application is deployed, it's available
at http://localhost:4444/contour_admin/
-> If port 4444 if forwarded on the router, the Tomcat server can be reached from outside the system, replacing
"localhost" with the IP of the host computer.

4.2.3
If you want to deploy the GWT application by yourself, perform the following steps:

1) Right click on project folder in Package Explorer -> Google -> GWT Compile -> select contour_admin as project and
hit compile -> wait for the modules to compile (should be 6 permutations)
2) After the compilation is finished:
	- open the "war" folder of the contour_admin project in the explorer
	- zip all the contents and rename the zip archive to "contour_admin.war"
	- copy "contour_admin.war" into Tomcat's "webapps" folder (Tomcat should be shut down at this point)
	- delete the "contour_admin" folder in the Tomcat's "webapps" folder
	- start the Tomcat server with the startup.bat in the "bin" folder -> the server is automatically
	  unpacking the web archive and deploying the application
	- if no errors occur, the application is deployed successfully and can be access by above mentioned URL

_____________________________________________________________________________________________________

5. Availability

-	The source code is available at https://j0h1@bitbucket.org/j0h1/contour-for-android.git
-	The source code documentation is available in the "doc" folders of each project
-	The author of Contour is reachable at e0926649@student.tuwien.ac.at

_____________________________________________________________________________________________________

6. Limitations

-	As an Android application, Contour is limited to Android devices. It can be used with both smartphones 
	and tablets with Jelly Bean (Android 4.1) or higher, which makes it available for 78.3 percent of all 
	Android devices worldwide.
-	The drawing experience on devices with lower resolutions is rather unsatisfying, especially without a stylus.
-	Contour depends on a stable internet connection. Offline mode is just partially available, when the application 
	was paused (not closed) during a drawing session and is continued to a later point, when no connection is available 
	anymore. This is, because all data needed for a drawing session is downloaded and stored locally when a session is 
	started. As soon as the session is finished and the player wants to save it, a internet connection is required again.
	
_____________________________________________________________________________________________________

7. Third-party software

7.1 Client-side

-	OpenCV v2.4.9 (http://opencv.org/)
-	Volley v1.0.6 (http://developer.android.com/training/volley/index.html)
-	SQLiteAssetHelper v2.0.1 (https://github.com/jgilfelt/android-sqlite-asset-helper)
-	Simple Framework v2.7.1 (http://simple.sourceforge.net/)
-	Apache-Commons-Lang3 v3.3.2 (http://commons.apache.org/proper/commons-lang/)
-	Jersey-Bundle v1.8 (https://jersey.java.net/index.html)
-	Android-Support v13 (http://developer.android.com/tools/support-library/index.html)

7.2 Server-side

7.2.2 contour_server

-	HSQLDB  v2.3.2 (http://hsqldb.org/)
-	Apache-Commons-Codec v1.9 (http://commons.apache.org/proper/commons-codec/)
-	Jersey-Bundle v1.8 (https://jersey.java.net/index.html)
-	JSR 311 v1.1 (https://jsr311.java.net/)
-	ASM v3.1 (http://asm.ow2.org/)

7.2.3 contour_server

-	OpenCV v2.4.9 (http://opencv.org/)
-	HSQLDB  v2.3.2 (http://hsqldb.org/)
-	GWTUpload v1.0.1 (https://code.google.com/p/gwtupload/)
-	Apache-Commons-IO v2.4 (http://commons.apache.org/proper/commons-io/)
-	Apache-Commons-FileUpload v1.3.1 (http://commons.apache.org/proper/commons-fileupload/)
-	Apache-Commons-Codec v1.9 (http://commons.apache.org/proper/commons-codec/)
