//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.dao;

import java.sql.SQLException;

import com.j0h1.contour.server.DatabaseManager;

/**
 * Encapsulates methods that are used by all DAO implementations.
 * @author Johannes Riedmann
 *
 */
public abstract class AbstractDAO {
	
	/**
	 * Opens the connection to the database.
	 */
	public void openConnection() {
		DatabaseManager.getInstance().openConnection();
	}
	
	/**
	 * Closes the connection to the database.
	 */
	public void closeConnection() {
		DatabaseManager.getInstance().closeConnection();
	}
	
	/**
	 * Enables auto-commit if parameter autoCommit is true, disables it otherwise.
	 * @param autoCommit To disable/enable auto-commit for database.
	 * @throws DAOException	If auto-commit could not be set, throw DAOException.
	 */
	public void setAutoCommit(boolean autoCommit) throws DAOException {
		try {
			if (DatabaseManager.getInstance().getConnection() != null) {
				DatabaseManager.getInstance().getConnection().setAutoCommit(autoCommit);
			}			
		} catch (SQLException ex) {
			throw new DAOException("Unable to set auto commit.");
		}
	}

}
