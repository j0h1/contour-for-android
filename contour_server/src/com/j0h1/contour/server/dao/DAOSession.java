//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.dao;

import java.util.ArrayList;

import com.j0h1.contour.server.rest.pojo.Image;
import com.j0h1.contour.server.rest.pojo.Session;

/**
 * Defines database access methods concerning Session objects.
 * @author Johannes Riedmann
 *
 */
public interface DAOSession {
	
	/**
	 * Queries for the 10 best track sessions (sessions with the highest score) of 
	 * a specific track and player in descending order (by score).
	 * @param playerId	If the playerId equals Integer.MAX_VALUE, no player will
	 * 					be specified and the overall top 10 sessions will be queried.
	 * 					If the playerId is specified and bigger than 0, the sessions for the
	 * 					given player are queried.
	 * @param trackId	ID of Track to find the top 10 sessions for.
	 * @return	Local or global top 10 track sessions in descending order (by score). If
	 * 			no sessions were found, an empty list is returned.
	 * @throws DAOSessionException	When the query fails, a DAOSessionException is thrown.
	 */
	public ArrayList<Session> getTrackSessions(int playerId, 
			int trackId) throws DAOSessionException;
	
	/**
	 * Queries for the 10 best custom sessions (sessions with the highest score) of 
	 * a specific player in descending order (by score).
	 * @param playerId	ID of player to find the custom sessions from.
	 * @return	Top 10 custom sessions of a specific player in descending order (by score).
	 * 			If no sessions were found, an empty list is returned.
	 * @throws DAOSessionException	When the query fails, a DAOSessionException is thrown.
	 */
	public ArrayList<Session> getCustomSessions(int playerId) 
			throws DAOSessionException;

	/**
	 * Persists a track session to the database.
	 * @param session		Session object to be persisted.
	 * @param playername	Unique name of the player that finished the session.
	 * @param drawings		List of drawings.
	 * @param scoreImages	List of generated score images.
	 * @throws DAOSessionException	When persistence fails, a DAOSessionException is thrown.
	 */
	public void persistTrackSession(Session session, String playername,
			ArrayList<Image> drawings, ArrayList<Image> scoreImages)
			throws DAOSessionException;

	/**
	 * Persists a custom session to the database.
	 * @param session		Session object to be persisted.
	 * @param playername	Unique name of the player that finished the session.
	 * @param originalData	Base64 encoded original image data.
	 * @param edgeData		Base64 encoded edge image data.
	 * @param imagename		Name of the image the player defined before the session.
	 * @param drawing		Image object that contains the drawing data.
	 * @param scoreImage	Image object that contains the score image data and the score
	 * 						achieved for the image.
	 * @throws DAOSessionException	When persistence fails, a DAOSessionException is thrown.
	 */
	public void persistCustomSession(Session session, String playername,
			String originalData, String edgeData, String imagename,
			Image drawing, Image scoreImage) throws DAOSessionException;

}
