//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.dao;

import java.util.ArrayList;

import com.j0h1.contour.server.rest.pojo.Image;

/**
 * Defines database access methods concerning Image objects.
 * @author Johannes Riedmann
 *
 */
public interface DAOImage {
	
	/**
	 * Queries for a list of edge images of a specific track and language.
	 * @param trackId	TrackId of the Track the edge images are assigned to.
	 * @param language	Language code ("en"/"de"), that defines if the image name
	 * 					should be returned in english ("en") or german ("de").
	 * @return	List of edge images in a given track in a specific language. Empty List if
	 * 			no Images were found.
	 * @throws DAOImageException	When the query fails, a DAOImageException is thrown.
	 */
	public ArrayList<Image> findEdgeImagesByTrackId(int trackId, String language) throws DAOImageException;
	
	/**
	 * Queries for the original image data for a given imageId.
	 * @param imageId	ID of the image to get the original data from.
	 * @return	Image object that contains the original image data for a given imageId. If image
	 * 			is not found, an empty Image object is returned.
	 * @throws DAOImageException	When the query fails, a DAOImageException is thrown.
	 */
	public Image getOriginalImageByImageId(int imageId) throws DAOImageException;
	
	/**
	 * Queries for the edge image data for a given imageId.
	 * @param imageId	ID of the image to get the edge data from.
	 * @return	Image object that contains the edge image data for a given imageId. If image
	 * 			is not found, an empty Image object is returned.
	 * @throws DAOImageException	When the query fails, a DAOImageException is thrown.
	 */
	public Image getEdgeImageByImageId(int imageId) throws DAOImageException;
	
	/**
	 * Queries for the user drawing data for specific image in a specific session.
	 * @param imageId	ID of the image to get the drawing data from.
	 * @param sessionId Id of the session, the drawing of the image was done.
	 * @return	Image object that contains the drawing data for a given imageId and sessionId.
	 * 			If image is not found, an empty Image object is returned.
	 * @throws DAOImageException	When the query fails, a DAOImageException is thrown.
	 */
	public Image getDrawingByImageIdAndSessionId(int imageId, int sessionId) throws DAOImageException;
	
	/**
	 * Queries for the score image data for specific image in a specific session.
	 * @param imageId	ID of the image to get the score image data from.
	 * @param sessionId	Id of the session, the score image of the drawing was generated.
	 * @return	Image object that contains the score image data for a given imageId and sessionId.
	 * 			If image is not found, an empty Image object is returned.
	 * @throws DAOImageException	When the query fails, a DAOImageException is thrown.
	 */
	public Image getScoreImageByImageIdAndSessionId(int imageId, int sessionId) throws DAOImageException;
	
	/**
	 * Retrieves the meta data (no image data) of all images persisted for a specific session in a
	 * given language (for the image names to be displayed in the correct language).
	 * @param sessionId	Id of the session to retreive details image metadata from.
	 * @param language	Language code ("en"/"de) to get the image names in the specified language.
	 * @return	Meta data of a session containing the images with the achieved scores. If no image
	 * 			meta data is found, an empty list is returned.
	 * @throws DAOImageException	When the query fails, a DAOImageException is thrown.
	 */
	public ArrayList<Image> getSessionDetails(int sessionId, String language) throws DAOImageException;
	
	/**
	 * Persists an new image to the database.
	 * @param titleEN			German name of the image.
	 * @param titleDE			English name of the image.
	 * @param originalContent	Original image data.
	 * @param edgeContent		Edge image data.
	 * @return	The generated ID of the image.
	 * @throws DAOImageException	When the persistence process fails, a DAOImageException is thrown.
	 */
	public int persist(String titleEN, String titleDE, String originalContent, String edgeContent) throws DAOImageException;
	
}
