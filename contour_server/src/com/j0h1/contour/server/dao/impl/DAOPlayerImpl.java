//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.j0h1.contour.server.DatabaseManager;
import com.j0h1.contour.server.dao.AbstractDAO;
import com.j0h1.contour.server.dao.DAOPlayer;
import com.j0h1.contour.server.dao.DAOPlayerException;
import com.j0h1.contour.server.rest.pojo.Player;

/**
 * Implementations for DAOPlayer.
 * @author Johannes Riedmann
 *
 */
public class DAOPlayerImpl extends AbstractDAO implements DAOPlayer {
	
	private Connection connection;
	private PreparedStatement prep;
	private DatabaseManager dbm;
	
	public DAOPlayerImpl() {
		connection = null;
		prep = null;
		dbm = DatabaseManager.getInstance();
	}
	
	@Override
	public Player findPlayerByPhoneId(String phoneId)
			throws DAOPlayerException {
		openConnection();
		connection = dbm.getConnection();
		
		try {
			prep = connection.prepareStatement("select playerId, playername "
					+ "from player "
					+ "where phoneId = ?");
			prep.setString(1, phoneId);
			
			ResultSet player = prep.executeQuery();
			
			Player ret = new Player();
			
			if (player.next()) {
				ret.setPlayerId(player.getInt("playerId"));
				ret.setPlayername(player.getString("playername"));
				ret.setPhoneId(phoneId);
			}
			
			player.close();
			prep.close();
			
			return ret;
		} catch (SQLException ex) {
			throw new DAOPlayerException("Unable to retrieve player.");
		} finally {
			closeConnection();
		}
	}

	@Override
	public Player persist(Player player) throws DAOPlayerException {
		openConnection();
		connection = dbm.getConnection();
		setAutoCommit(false);
		
		try {
			prep = connection.prepareStatement("insert into "
					+ "player(playername, phoneId) "
					+ "values(?, ?)", 
					Statement.RETURN_GENERATED_KEYS);
			prep.setString(1, player.getPlayername());
			prep.setString(2, player.getPhoneId());
			prep.executeUpdate();
			
			ResultSet playerId = prep.getGeneratedKeys();
			
			Player ret = new Player();
			
			if (playerId.next()) {
				ret.setPlayerId(playerId.getInt(1));
				ret.setPlayername(player.getPlayername());
				ret.setPhoneId(player.getPhoneId());
			}
			
			playerId.close();
			prep.close();
			
			connection.commit();
			
			return ret;
		} catch (SQLException ex) {
			throw new DAOPlayerException("Playername already in use.");
		} finally {
			closeConnection();
		}
	}

}
