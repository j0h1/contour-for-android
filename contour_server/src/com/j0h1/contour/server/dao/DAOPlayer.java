//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.dao;

import com.j0h1.contour.server.rest.pojo.Player;

/**
 * Defines database access methods concerning Player objects.
 * @author Johannes Riedmann
 *
 */
public interface DAOPlayer {
	
	/**
	 * Queries the database for a player with a specific phoneId.
	 * @param phoneId	Unique Android device identifier for the player's phone.
	 * @return	Fully specified Player object. Empty Player object is player was not found.
	 * @throws DAOPlayerException	When the query fails, a DAOPlayerException is thrown.
	 */
	public Player findPlayerByPhoneId(String phoneId) throws DAOPlayerException;
	
	/**
	 * Persists a new Player to the database.
	 * @param player	Player object to be persisted.
	 * @return	Fully specified Player object. Empty Player object is player was not persisted
	 * 			successfully.
	 * @throws DAOPlayerException	When persistence fails, a DAOPlayerException is thrown.
	 */
	public Player persist(Player player) throws DAOPlayerException;

}
