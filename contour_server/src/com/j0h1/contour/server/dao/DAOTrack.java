//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.dao;

import java.util.ArrayList;

import com.j0h1.contour.server.rest.pojo.Track;

/**
 * Defines database access methods concerning Track objects.
 * @author Johannes Riedmann
 *
 */
public interface DAOTrack {
	
	/**
	 * This method returns all non-deleted tracks in a certain language.
	 * @param language 	Language code ("en"/"de") to define the language the 
	 * 					track names are returned in.
	 * @return 	List of all non-deleted tracks defined in the database. If no
	 * 			tracks are persisted, an empty list is returned.
	 */
	public ArrayList<Track> findAll(String language) throws DAOTrackException;
	
}
