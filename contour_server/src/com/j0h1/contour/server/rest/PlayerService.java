//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.rest;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.j0h1.contour.server.dao.DAOPlayer;
import com.j0h1.contour.server.dao.DAOPlayerException;
import com.j0h1.contour.server.dao.impl.DAOPlayerImpl;
import com.j0h1.contour.server.rest.pojo.Player;

/**
 * The methods of this service class are invoked by requesting the defined URL paths.
 * The response of this service is always a single Player object.
 * @author Johannes Riedmann
 *
 */
@Path("players")
public class PlayerService {
	
	/**
	 * This method responds to it's specified path by generating a XML document
	 * containing the Player object that is assigned to the given phoneId.
	 * @param phoneId	Unique Android device identifier that identifies a phone.
	 * @return	Responds with a Player object, mapped to a XML document. If no player
	 * 			was found, the XML document only contains an empty player object.
	 */
	@GET
	@Path("phoneId/{phoneId}")
	@Produces(MediaType.APPLICATION_XML)
	public Player findPlayerByPhoneId(@PathParam("phoneId") String phoneId) {
		DAOPlayer playerDao = new DAOPlayerImpl();
		
		Player player = new Player();
		
		try {
			player = playerDao.findPlayerByPhoneId(phoneId);
		} catch (DAOPlayerException ex) {
			// feedback on HTTP service not needed, because it should
			// be running without supervision
		}
		
		return player;
	}
	
	/**
	 * This method responds to it's specified path by generating a XML document
	 * containing the Player object that was persisted to the database.
	 * @param formParam	Parameters that are passed to this request. In this case
	 * 					the player name and the phoneId that have to be persisted.
	 * @return	Responds with a Player object, mapped to a XML document. If the player
	 * could not be persisted, the XML document only contains an empty player object.
	 */
	@POST
	@Produces(MediaType.APPLICATION_XML)
	public Player persist(MultivaluedMap<String, String> formParam) {		
		Player player = new Player();
		player.setPlayername(formParam.getFirst("playername"));
		player.setPhoneId(formParam.getFirst("phoneId"));
		
		DAOPlayer playerDao = new DAOPlayerImpl();
		
		try {
			player = playerDao.persist(player);
		} catch (DAOPlayerException ex) {
			// feedback on HTTP service not needed, because it should
			// be running without supervision
		}
		
		return player;
	}

}
