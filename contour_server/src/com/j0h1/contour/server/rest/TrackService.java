//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.rest;

import java.util.ArrayList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

import com.j0h1.contour.server.dao.DAOTrackException;
import com.j0h1.contour.server.dao.impl.DAOTrackImpl;
import com.j0h1.contour.server.rest.pojo.Track;
import com.j0h1.contour.server.rest.pojo.TrackResponseList;

/**
 * The methods of this service class are invoked by requesting the defined URL paths.
 * The response of this service is either a single Track object or a List of Track objects.
 * @author Johannes Riedmann
 *
 */
@Path("tracks")
public class TrackService {
	
	/**
	 * This method responds to it's specified path by generating a XML document
	 * containing a list of all Track objects persisted in the database.
	 * @param language	Language code ("en"/"de") to retrieve the track names
	 * 					in the correct language.
	 * @return	Responds with a list of all Track objects mapped to a XML document.
	 * 			If there are no tracks persisted, an empty list is returned.
	 */
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("language/{language}")
	public TrackResponseList getAllCategories(@PathParam("language") String language) {
		DAOTrackImpl trackDao = new DAOTrackImpl();
		
		TrackResponseList trackList = new TrackResponseList();
		ArrayList<Track> tracks = new ArrayList<Track>();
		
		try {
			tracks = trackDao.findAll(language);
		} catch (DAOTrackException ex) {
			// feedback on HTTP service not needed, because it should
			// be running without supervision
		}
		
		trackList.setList(tracks);
		
		return trackList;
	}
	
}
