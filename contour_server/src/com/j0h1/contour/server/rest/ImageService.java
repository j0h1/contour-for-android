//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.rest;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.j0h1.contour.server.dao.DAOImage;
import com.j0h1.contour.server.dao.DAOImageException;
import com.j0h1.contour.server.dao.impl.DAOImageImpl;
import com.j0h1.contour.server.rest.pojo.Image;
import com.j0h1.contour.server.rest.pojo.ImageResponseList;

/**
 * The methods of this service class are invoked by requesting the defined URL paths.
 * The response of this service is either a single Image object or a List of Image objects.
 * @author Johannes Riedmann
 *
 */
@Path("images")
public class ImageService {
	
	/**
	 * This method responds to it's specified path by generating a XML document
	 * containing all edge images for a given trackId in a specified language.
	 * @param trackId	ID of track to find edge images from.
	 * @param language	Language code ("en"/"de") to retrieve the image names
	 * 					in the correct language.
	 * @return	ImageResponseList that contains all edge images for the current track
	 * 			in the specified language. If no edge images are found, a empty list
	 * 			is returned.
	 */
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("track/{trackId}/language/{language}")
	public ImageResponseList findEdgeImagesByTrackId(@PathParam("trackId") int trackId, 
			@PathParam("language") String language) {
		DAOImage imageDao = new DAOImageImpl();
		
		ArrayList<Image> edgeImages = new ArrayList<Image>();
		
		try {
			edgeImages = imageDao.findEdgeImagesByTrackId(trackId, language);
		} catch (DAOImageException ex) {
			// feedback on HTTP service not needed, because it should
			// be running without supervision
		}
		
		ImageResponseList edgeImageList = new ImageResponseList();
		edgeImageList.setList(edgeImages);
		
		return edgeImageList;
	}
	
	/**
	 * This method responds to it's specified path by generating a XML document
	 * containing image meta data for a given sessionId in a specified language.
	 * @param sessionId	ID of session to get the image meta data from.
	 * @param language	Language code ("en"/"de") to retrieve the image names
	 * 					in the correct language.
	 * @return	ImageResponseList that contains image meta data for the specified session
	 * 			in the given language. If no images are found, a empty list
	 * 			is returned.
	 */
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("session/{sessionId}/language/{language}")
	public ImageResponseList getSessionDetails(@PathParam("sessionId") int sessionId, 
			@PathParam("language") String language) {
		DAOImage imageDao = new DAOImageImpl();
		
		ArrayList<Image> imageMetaData = new ArrayList<Image>();
		
		try {
			imageMetaData = imageDao.getSessionDetails(sessionId, language);
		} catch (DAOImageException ex) {
			// feedback on HTTP service not needed, because it should
			// be running without supervision
		}
		
		ImageResponseList sessionDetails = new ImageResponseList();		
		sessionDetails.setList(imageMetaData);
		
		return sessionDetails;
	}
	
	/**
	 * This method responds to it's specified path by generating a XML document
	 * containing image data for a given imageId, sessionId and type.
	 * @param imageId	ID of image.
	 * @param sessionId	ID of session the image is associated with.
	 * @param type		0 = original image data requested
	 * 					1 = edge image data requested
	 * 					2 = user drawing data requested
	 * 					3 = score image data requested
	 * @return	Responds with an Image object, mapped to a XML document, that
	 * 			contains image data for the requested image. If no image was
	 * 			found, an empty Image object is returned.
	 */
	@GET
	@Produces(MediaType.APPLICATION_XML)
	@Path("image/{imageId}/session/{sessionId}/type/{type}")
	public Image getImageByType(@PathParam("imageId") int imageId, 
			@PathParam("sessionId") int sessionId, @PathParam("type") int type) {
		DAOImage imageDao = new DAOImageImpl();
		
		Image image = new Image();
		
		try {
			if (type == 0) {
				// request for original image
				image = imageDao.getOriginalImageByImageId(imageId);
			} else if (type == 1) {
				// request for edge image
				image = imageDao.getEdgeImageByImageId(imageId);
			} else if (type == 2) {
				// request for user drawing
				image = imageDao.getDrawingByImageIdAndSessionId(imageId, sessionId);
			} else if (type == 3) {
				// request for score image
				image = imageDao.getScoreImageByImageIdAndSessionId(imageId, sessionId);
			}
		} catch (DAOImageException ex) {
			// feedback on HTTP service not needed, because it should
			// be running without supervision
		}
		
		return image;
	}
	
}
