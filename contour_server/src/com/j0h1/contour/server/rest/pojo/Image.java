//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.rest.pojo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Serializable representation of an Image object, that can be mapped
 * to a XML document.
 * @author Johannes Riedmann
 *
 */
@XmlRootElement(name = "image")
public class Image implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// id of the image
	private int imageId;
	// name of the image
	private String imagename;
	// position of the image in the track
	private int position;
	// score of the image if it's an score image
	private double score;
	// image data
	private byte[] data;
	
	public Image() {
		
	}

	@XmlElement(name = "imageId", required = false)
	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	@XmlElement(name = "imagename", required = false)
	public String getImagename() {
		return imagename;
	}

	public void setImagename(String imagename) {
		this.imagename = imagename;
	}
	
	@XmlElement(name = "position", required = false)
	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	@XmlElement(name = "data", required = false)
	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	@XmlElement(name = "score", required = false)
	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
	
}
