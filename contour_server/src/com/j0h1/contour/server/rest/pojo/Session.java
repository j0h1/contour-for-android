//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.server.rest.pojo;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Serializable representation of a Session object, that can be mapped
 * to a XML document.
 * @author Johannes Riedmann
 *
 */
@XmlRootElement(name = "session")
public class Session implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// id of session
	private int sessionId;
	// Player that finished this session
	private Player player;
	// Track for this session (null if custom session)
	private Track track;
	// score achieved in this session
	private double score;
	// date of this session in milliseconds
	private long date;
	
	public Session() {
		
	}

	@XmlElement(name = "sessionId", required = false)
	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	@XmlElement(name = "score", required = false)
	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	@XmlElement(name = "date", required = false)
	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	@XmlElement(name = "track", required = false)
	public Track getTrack() {
		return track;
	}

	public void setTrack(Track track) {
		this.track = track;
	}

	@XmlElement(name = "player", required = false)
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

}
