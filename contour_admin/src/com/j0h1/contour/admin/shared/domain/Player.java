//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.shared.domain;

import java.io.Serializable;

/**
 * Player object representation that encapsulates
 * data to define a player.
 * @author Johannes Riedmann
 *
 */
public class Player implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// id of player
	private int playerId;
	// username of player
	private String playername;
	// unique identifier for player's device
	private String phoneId;
	
	public Player() {
		
	}
	
	public Player(int playerId, String playername) {
		this.playerId = playerId;
		this.playername = playername;
	}
	
	public Player(int playerId, String playername, String phoneId) {
		this.playerId = playerId;
		this.playername = playername;
		this.phoneId = phoneId;
	}

	public int getPlayerId() {
		return playerId;
	}

	public void setPlayerId(int playerId) {
		this.playerId = playerId;
	}

	public String getPlayername() {
		return playername;
	}

	public void setPlayername(String playername) {
		this.playername = playername;
	}

	public String getPhoneId() {
		return phoneId;
	}

	public void setPhoneId(String phoneId) {
		this.phoneId = phoneId;
	}

}
