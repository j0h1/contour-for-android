//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.shared.domain;

import java.io.Serializable;

/**
 * Session object representation that encapsulates
 * data to define a session.
 * @author Johannes Riedmann
 *
 */
public class Session implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// id of session
	private int sessionId;
	// Player that finished this session
	private Player player;
	// Track for this session (null if custom session)
	private Track track;
	// score achieved in this session
	private float score;
	// date of this session in milliseconds
	private long date;
	
	public Session() {
		
	}
	
	public Session(int sessionId, Player player, Track track, float score, long date) {
		this.sessionId = sessionId;
		this.setPlayer(player);
		this.track = track;
		this.score = score;
		this.date = date;
	}
	
	public Track getTrack() {
		return this.track;
	}
	
	public void addToScore(float value) {
		this.score += value;
	}
	
	public float getScore() {
		return this.score;
	}

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}
	
}
