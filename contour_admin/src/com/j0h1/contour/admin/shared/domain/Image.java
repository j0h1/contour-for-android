//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.shared.domain;

import java.io.Serializable;

/**
 * Image object representation that contains an 
 * images meta data to show/list it in the application.
 * @author Johannes Riedmann
 *
 */
public class Image implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// id of the image
	private int imageId;
	// english title of the image
	private String titleEN;
	// flag indicating if image in track
	private boolean inTrack;
	// position of image in a track
	private int position;
	
	public Image() {
		
	}

	public int getImageId() {
		return imageId;
	}

	public void setImageId(int imageId) {
		this.imageId = imageId;
	}

	public String getTitleEN() {
		return titleEN;
	}

	public void setTitleEN(String titleEN) {
		this.titleEN = titleEN;
	}

	public boolean isInTrack() {
		return inTrack;
	}

	public void setInTrack(boolean inTrack) {
		this.inTrack = inTrack;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

}
