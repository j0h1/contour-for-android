//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.shared.domain;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Track object representation that encapsulates
 * data to define a track.
 * @author Johannes Riedmann
 *
 */
public class Track implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	// id of track
	private int trackId;
	// english title of track
	private String titleEN;
	// german title of track
	private String titleDE;
	// images assigned to this track
	private ArrayList<Image> images;
	
	public Track() {
		
	}

	public String getTitleEN() {
		return titleEN;
	}

	public void setTitleEN(String titleEN) {
		this.titleEN = titleEN;
	}

	public String getTitleDE() {
		return titleDE;
	}

	public void setTitleDE(String titleDE) {
		this.titleDE = titleDE;
	}

	public ArrayList<Image> getImages() {
		return images;
	}

	public void setImages(ArrayList<Image> images) {
		this.images = images;
	}

	public int getTrackId() {
		return trackId;
	}

	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}

}
