//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.StackPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Container for Views that deal with player management.
 * @author Johannes Riedmann
 *
 */
public class PlayerContainer extends Composite {

	private static PlayerContainerUiBinder uiBinder = GWT
			.create(PlayerContainerUiBinder.class);
	
	@UiField StackPanel spnPlayerView;
	
	private DeletePlayerView deletePlayerView;

	interface PlayerContainerUiBinder extends UiBinder<Widget, PlayerContainer> {
	}

	public PlayerContainer() {
		initWidget(uiBinder.createAndBindUi(this));
		
		deletePlayerView = new DeletePlayerView();
		
		spnPlayerView.add(deletePlayerView, "Delete Player");
	}
	
	public DeletePlayerView getDeletePlayerView() {
		return deletePlayerView;
	}

}
