//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.SelectionEvent;
import com.google.gwt.event.logical.shared.SelectionHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabBar;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.TabPanel;

/**
 * Parent container for image-, track-, player- and 
 * session-management containers.
 * @author Johannes Riedmann
 *
 */
public class UiContainer extends Composite implements EntryPoint {

	private static UiContainerUiBinder uiBinder = GWT
			.create(UiContainerUiBinder.class);
	
	@UiField TabPanel tpnContainer;
	
	private ImageContainer imageContainer;
	private TrackContainer trackContainer;
	private PlayerContainer playerContainer;
	private SessionContainer sessionContainer;

	interface UiContainerUiBinder extends UiBinder<Widget, UiContainer> {
	}

	public UiContainer() {
		initWidget(uiBinder.createAndBindUi(this));
		
		imageContainer = new ImageContainer();
		trackContainer = new TrackContainer();
		playerContainer = new PlayerContainer();
		sessionContainer = new SessionContainer();
		
		// add widgets to containers		
		tpnContainer.add(imageContainer, "Images");
		tpnContainer.add(trackContainer, "Tracks");
		tpnContainer.add(playerContainer, "Players");
		tpnContainer.add(sessionContainer, "Sessions");
		
		// reload Views when switching tabs (refresh data)
		final TabBar bar = tpnContainer.getTabBar();
		bar.addSelectionHandler(new SelectionHandler<Integer>() {
			@Override
			public void onSelection(SelectionEvent<Integer> event) {
				if (event.getSelectedItem() == 0) {
					// do nothing
				} else if (event.getSelectedItem() == 1) {
					// refresh data and show first panel
					trackContainer.getAddTrackView().refreshData();
					trackContainer.getContainer().showStack(0);
				} else if (event.getSelectedItem() == 2) {
					// do nothing
				} else if (event.getSelectedItem() == 3) {
					sessionContainer.getDeleteSessionView().refreshData();
				}
			}			
		});
		
		tpnContainer.selectTab(0);
	}

	@Override
	public void onModuleLoad() {
		RootPanel.get().add(new UiContainer());
	}

}
