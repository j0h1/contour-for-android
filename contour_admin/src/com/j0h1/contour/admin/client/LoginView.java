//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.event.dom.client.KeyDownHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.user.client.ui.PasswordTextBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Label;

/**
 * View that contains the UI elements and logic for a simple login.
 * @author Johannes Riedmann
 *
 */
public class LoginView extends Composite implements EntryPoint {

	private static LoginViewUiBinder uiBinder = GWT
			.create(LoginViewUiBinder.class);
	
	@UiField PasswordTextBox txtPassword;
	@UiField TextBox txtUsername;
	@UiField Button btnLogin;
	@UiField Label lblStatus;

	interface LoginViewUiBinder extends UiBinder<Widget, LoginView> {
	}

	public LoginView() {
		initWidget(uiBinder.createAndBindUi(this));
		
		KeyDownHandler enterKeyHandler = new KeyDownHandler() {
			@Override
			public void onKeyDown(KeyDownEvent event) {
				if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
					btnLogin.click();
				}
			}			
		};
		
		txtUsername.addKeyDownHandler(enterKeyHandler);
		txtPassword.addKeyDownHandler(enterKeyHandler);
	}

	public void onModuleLoad() {
		LoginView lv = new LoginView();
		
		RootPanel.get().add(lv);
	}
	
	@UiHandler("btnLogin")
	public void loginClicked(ClickEvent e) {
		if (txtUsername.getText().equals("ContourAdmin") && txtPassword.getText().equals("j0h1PW2014")) {
			
			// change to logged in views
			RootPanel.get().clear();
			RootPanel.get().add(new UiContainer());
		} else {
			lblStatus.setText("Wrong username or password.");
			lblStatus.setVisible(true);
		}
	}

}
