//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client;

import java.util.ArrayList;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.j0h1.contour.admin.client.service.TrackService;
import com.j0h1.contour.admin.client.service.TrackServiceAsync;
import com.j0h1.contour.admin.shared.domain.Track;

/**
 * View that contains the UI elements and logic to delete an existing track.
 * @author Johannes Riedmann
 *
 */
public class DeleteTrackView extends Composite {

	private static DeleteTrackViewUiBinder uiBinder = GWT
			.create(DeleteTrackViewUiBinder.class);
	
	@UiField FlexTable ftbDeleteTracks;
	@UiField Label lblStatus;

	interface DeleteTrackViewUiBinder extends UiBinder<Widget, DeleteTrackView> {		
	}

	public DeleteTrackView() {
		initWidget(uiBinder.createAndBindUi(this));
		
		refreshData();
	}
	
	private void setFlexTableHeaderData() {
		ftbDeleteTracks.setText(0, 0, "TrackID");
		ftbDeleteTracks.setText(0, 1, "Title (en)");
		ftbDeleteTracks.setText(0, 2, "Delete?");
	}
	
	public void refreshData() {
		ftbDeleteTracks.removeAllRows();
		
		setFlexTableHeaderData();
		
		// call remote service to get tracks
		TrackServiceAsync trackService = GWT.create(TrackService.class);
		
		AsyncCallback<ArrayList<Track>> callback = new AsyncCallback<ArrayList<Track>>() {

			@Override
			public void onFailure(Throwable caught) {
				setStatusLabel(caught.getMessage(), true);
			}

			@Override
			public void onSuccess(ArrayList<Track> result) {
				populateFlexTable(result);
			}			
		};
		
		trackService.findAll(callback);
	}
	
	private void populateFlexTable(ArrayList<Track> tracks) {
		for (int i = 0; i < tracks.size(); i++) {
			// Button to delete Image
			Button removeTrack = new Button("x");
			removeTrack.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					final int rowIndex = ftbDeleteTracks.getCellForEvent(event).getRowIndex();
					
					// extracting imageId from FlexTable
					String trackIdString = ftbDeleteTracks.getText(rowIndex, 0);
					int trackId = Integer.valueOf(trackIdString);
					
					// handling callback from TrackService
					TrackServiceAsync trackService = GWT.create(TrackService.class);
					
					AsyncCallback<Void> callback = new AsyncCallback<Void>() {
						public void onFailure(Throwable caught) {
							setStatusLabel(caught.getMessage(), true);
						}

						public void onSuccess(Void result) {
							// FlexTable is automatically reloaded after clicking on it,
							// so no need to remove the row
							setStatusLabel("Track successfully deleted.", false);
						}
					};
					
					// calling ImageService to delete image with specified id
					trackService.delete(trackId, callback);
				}
			});
			
			ftbDeleteTracks.setText(i + 1, 0, String.valueOf(tracks.get(i).getTrackId()));
			ftbDeleteTracks.setText(i + 1, 1, tracks.get(i).getTitleEN());
			ftbDeleteTracks.setWidget(i + 1, 2, removeTrack);
		}		
	}
	
	/**
	 * Set the status label to a given text for feedback.
	 * @param text		Exception/success message.
	 * @param isError 	Flag that defines if the current message is a
	 * 					error message.
	 */
	private void setStatusLabel(String text, boolean isError) {
		lblStatus.setVisible(true);
		lblStatus.setText(text);
		
		if (isError) {
			lblStatus.getElement().getStyle().setColor("red");
		} else {
			lblStatus.getElement().getStyle().setColor("green");
		}
		
		Timer t = new Timer() {
			public void run() {
				lblStatus.setVisible(false);
			}
		};

		t.schedule(3000);
	}

}
