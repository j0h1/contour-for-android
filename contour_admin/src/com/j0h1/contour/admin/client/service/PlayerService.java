//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.j0h1.contour.admin.shared.DAOPlayerException;
import com.j0h1.contour.admin.shared.domain.Player;

/**
 * Remote service interface, defining methods to handle RPC calls from the client,
 * concerning Player objects.
 * @author Johannes Riedmann
 *
 */
@RemoteServiceRelativePath("players")
public interface PlayerService extends RemoteService {
	
	/**
	 * Returns all players persisted in the database.
	 * @return	List of all Player objects.
	 * @throws DAOPlayerException	If the query fails, specific feedback
	 * 								is provided.
	 */
	public ArrayList<Player> findAll() throws DAOPlayerException;
	
	/**
	 * Deletes a player from the database. If the player had sessions persisted
	 * in the database, they are deleted to, along with the images associated with
	 * them.
	 * @param playerId	ID of player to be deleted.
	 * @throws DAOPlayerException	If deletion fails, specific feedback
	 * 								is provided.
	 */
	public void delete(int playerId) throws DAOPlayerException;

}
