//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.j0h1.contour.admin.shared.DAOSessionException;
import com.j0h1.contour.admin.shared.domain.Session;

/**
 * Remote service interface, defining methods to handle RPC calls from the client,
 * concerning Session objects.
 * @author Johannes Riedmann
 *
 */
@RemoteServiceRelativePath("sessions")
public interface SessionService extends RemoteService {
	
	/**
	 * Returns all sessions persisted in the database.
	 * @return	List of all Session objects.
	 * @throws DAOSessionException	If the query fails, specific feedback
	 * 								is provided.
	 */
	public ArrayList<Session> findAll() throws DAOSessionException;
	
	/**
	 * Deletes the session with the given sessionId from the database.
	 * If it's a custom session, the original data from the image is
	 * deleted with the session.
	 * @param sessionId	ID of session to be deleted.
	 * @throws DAOSessionException	If deletion fails, specific feedback
	 * 								is provided
	 */
	public void delete(int sessionId) throws DAOSessionException;
	
}
