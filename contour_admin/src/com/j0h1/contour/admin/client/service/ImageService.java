//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.j0h1.contour.admin.shared.DAOImageException;
import com.j0h1.contour.admin.shared.domain.Image;

/**
 * Remote service interface, defining methods to handle RPC calls from the client,
 * concerning Image objects.
 * @author Johannes Riedmann
 *
 */
@RemoteServiceRelativePath("images")
public interface ImageService extends RemoteService {
	
	/**
	 * Calls DAOImage to persist an image, along with its edge image to the database.
	 * @param titleEN 		English title of image.
	 * @param titleDE 		German title of image.
	 * @param originalImage Content of the original image (pre-images-processing).
	 * @param edgeImage 	Content of edge image.
	 * @throws DAOImageException 	If persistence fails to write images to database,
	 * 								provide specific feedback.
	 */
	public void persist(String titleEN, String titleDE, String originalImage, 
			String edgeImage) throws DAOImageException;
	
	/**
	 * Calls DAOImage to delete an image.
	 * @param imageId ID of image to be deleted.
	 * @throws DAOImageException If deletion of image fails (provides specific feedback).
	 */
	public void delete(int imageId) throws DAOImageException;
	
	/**
	 * Generates base64 encoded edge image content, based on an input image and a threshold
	 * using OpenCV.
	 * @param originalImage Content data of original image (pre-image-processing).
	 * @param fileEnding 	Type of image file (jpg/png) needed for OpenCV image encoding.
	 * @param threshold 	Lower threshold used by OpenCV's edge detection algorithm.
	 * @return Base64 encoded content data of edge image.
	 */
	public String generateEdgeImage(String originalImage, 
			String fileEnding, int threshold);
	
	/**
	 * Returns all non-deleted images (deleted flag not set) persisted on database.
	 * @return List of non-deleted Image objects
	 * @throws DAOImageException if DAOImage fails to receive information from database
	 */
	public ArrayList<Image> findAll() throws DAOImageException;

}
