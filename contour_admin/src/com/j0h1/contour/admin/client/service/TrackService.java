//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client.service;

import java.util.ArrayList;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;
import com.j0h1.contour.admin.shared.DAOTrackException;
import com.j0h1.contour.admin.shared.domain.Track;

/**
 * Remote service interface, defining methods to handle RPC calls from the client,
 * concerning Track objects.
 * @author Johannes Riedmann
 *
 */
@RemoteServiceRelativePath("tracks")
public interface TrackService extends RemoteService {
	
	/**
	 * Calls DAOTrack server-sided to persist a Track to the database.
	 * @param track	Track object to be persisted to database.
	 * @throws DAOTrackException 	If persisting image fails, provide
	 * 								specific feedback
	 */
	public void persist(Track track) throws DAOTrackException;
	
	/**
	 * Calls DAOTrack to delete a track from the database.
	 * @param trackId ID of Track to be deleted.
	 * @throws DAOTrackException	If deletion of image fails, 
	 * 								provide specific feedback
	 */
	public void delete(int trackId) throws DAOTrackException;
	
	/**
	 * Calls DAOTrack to return all tracks persisted in the database.
	 * @return List containing all Track objects persisted in the database.
	 * @throws DAOTrackException 	If query fails, provide specific feedback
	 */
	public ArrayList<Track> findAll() throws DAOTrackException;
	
}
