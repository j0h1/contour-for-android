//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.StackPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * Container for Views that deal with track management.
 * @author Johannes Riedmann
 *
 */
public class TrackContainer extends Composite {

	private static TrackContainerUiBinder uiBinder = GWT
			.create(TrackContainerUiBinder.class);
	
	@UiField StackPanel spnTrackView;
	
	private AddTrackView addTrackView;
	private DeleteTrackView deleteTrackView;

	interface TrackContainerUiBinder extends UiBinder<Widget, TrackContainer> {
		
	}

	public TrackContainer() {
		initWidget(uiBinder.createAndBindUi(this));
		
		addTrackView = new AddTrackView();
		deleteTrackView = new DeleteTrackView();
		
		spnTrackView.add(addTrackView, "Add Track");
		spnTrackView.add(deleteTrackView, "Delete Track");
		
		spnTrackView.addHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				// when switching to delete tracks, refresh data
				if (spnTrackView.getSelectedIndex() == 1) {
					deleteTrackView.refreshData();
				}
			}
		}, ClickEvent.getType());
	}
	
	public AddTrackView getAddTrackView() {
		return addTrackView;
	}
	
	public DeleteTrackView getDeleteTrackView() {
		return deleteTrackView;
	}
	
	public StackPanel getContainer() {
		return spnTrackView;
	}

}
