//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client;

import java.util.ArrayList;
import java.util.Date;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;
import com.j0h1.contour.admin.client.service.SessionService;
import com.j0h1.contour.admin.client.service.SessionServiceAsync;
import com.j0h1.contour.admin.shared.domain.Session;

/**
 * View that contains the UI elements and logic to delete an existing session.
 * @author Johannes Riedmann
 *
 */
public class DeleteSessionView extends Composite {
	
	@UiField FlexTable ftbDeleteSession;
	@UiField Label lblStatus;

	private static DeleteSessionViewUiBinder uiBinder = GWT
			.create(DeleteSessionViewUiBinder.class);

	interface DeleteSessionViewUiBinder extends
			UiBinder<Widget, DeleteSessionView> {
	}

	public DeleteSessionView() {
		initWidget(uiBinder.createAndBindUi(this));
		
		refreshData();
	}
	
	private void setFlexTableHeaderData() {
		ftbDeleteSession.setText(0, 0, "Session ID");
		ftbDeleteSession.setText(0, 1, "Player");
		ftbDeleteSession.setText(0, 2, "Track");
		ftbDeleteSession.setText(0, 3, "Date");
		ftbDeleteSession.setText(0, 4, "Score");
	}
	
	public void refreshData() {
		ftbDeleteSession.removeAllRows();
		
		setFlexTableHeaderData();
		
		SessionServiceAsync sessionService = GWT.create(SessionService.class);
		
		AsyncCallback<ArrayList<Session>> callback = new AsyncCallback<ArrayList<Session>>() {
			@Override
			public void onFailure(Throwable caught) {
				lblStatus.setText(caught.getMessage());
			}

			@Override
			public void onSuccess(ArrayList<Session> result) {
				populateFlexTable(result);
				lblStatus.setText("");
			}			
		};
		
		sessionService.findAll(callback);
	}
	
	private void populateFlexTable(ArrayList<Session> sessions) {
		for (int i = 0; i < sessions.size(); i++) {
			Button removeButton = new Button("x");
			removeButton.addClickHandler(new ClickHandler() {
				@Override
				public void onClick(ClickEvent event) {
					final int rowIndex = ftbDeleteSession.getCellForEvent(event).getRowIndex();
					
					// delete session
					String sessionIdString = ftbDeleteSession.getText(rowIndex, 0);
					final int sessionId = Integer.valueOf(sessionIdString);
					
					// call service and delete
					SessionServiceAsync sessionService = GWT.create(SessionService.class);
					
					AsyncCallback<Void> callback = new AsyncCallback<Void>() {
						@Override
						public void onFailure(Throwable caught) {
							setStatusLabel(caught.getMessage(), true);
						}

						@Override
						public void onSuccess(Void result) {
							ftbDeleteSession.removeRow(rowIndex);
							setStatusLabel("Successfully removed session with id " + sessionId, false);
						}						
					};
					
					sessionService.delete(sessionId, callback);
				}				
			});
			
			DateTimeFormat formatter = DateTimeFormat.getFormat("yyyy-MM-dd HH:mm:ss");
			
			ftbDeleteSession.setText(i + 1, 0, String.valueOf(sessions.get(i).getSessionId()));
			ftbDeleteSession.setText(i + 1, 1, sessions.get(i).getPlayer().getPlayername());
			if (sessions.get(i).getTrack().getTitleEN() == null) {
				ftbDeleteSession.setText(i + 1, 2, "custom");
			} else {
				ftbDeleteSession.setText(i + 1, 2, sessions.get(i).getTrack().getTitleEN());
			}			
			ftbDeleteSession.setText(i + 1, 3, formatter.format(new Date(sessions.get(i).getDate())));
			ftbDeleteSession.setText(i + 1, 4, String.valueOf(sessions.get(i).getScore()));
			ftbDeleteSession.setWidget(i + 1, 5, removeButton);
		}
	}
	
	/**
	 * Set the status label to a given text for feedback.
	 * @param text		Exception/success message.
	 * @param isError 	Flag that defines if the current message is a
	 * 					error message.
	 */
	private void setStatusLabel(String text, boolean isError) {
		lblStatus.setVisible(true);
		lblStatus.setText(text);
		
		if (isError) {
			lblStatus.getElement().getStyle().setColor("red");
		} else {
			lblStatus.getElement().getStyle().setColor("green");
		}
		
		Timer t = new Timer() {
			public void run() {
				lblStatus.setVisible(false);
			}
		};

		t.schedule(3000);
	}

}
