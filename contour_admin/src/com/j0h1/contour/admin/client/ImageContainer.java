//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Widget;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.StackPanel;

/**
 * Container for Views that deal with image management.
 * @author Johannes Riedmann
 *
 */
public class ImageContainer extends Composite {

	private static ImageContainerUiBinder uiBinder = GWT
			.create(ImageContainerUiBinder.class);
	
	@UiField StackPanel spnImageView;
	
	private AddImageView addImageView;
	private DeleteImageView deleteImageView;

	interface ImageContainerUiBinder extends
			UiBinder<Widget, ImageContainer> {
	}

	public ImageContainer() {
		initWidget(uiBinder.createAndBindUi(this));
		
		addImageView = new AddImageView();
		deleteImageView = new DeleteImageView();
		
		spnImageView.add(addImageView, "Add Image");
		spnImageView.add(deleteImageView, "Delete Image");
		
		spnImageView.addHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				if (spnImageView.getSelectedIndex() == 1) {
					// refresh image list
					deleteImageView.refreshData();
				}
			}
		}, ClickEvent.getType());
	}

}
