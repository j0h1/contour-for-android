//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.server.dao;

import java.util.ArrayList;

import com.j0h1.contour.admin.shared.DAOTrackException;
import com.j0h1.contour.admin.shared.domain.Track;

/**
 * Defines database access methods concerning Track objects.
 * @author Johannes Riedmann
 *
 */
public interface DAOTrack {
	
	/**
	 * Persists a track to the database.
	 * @param track Track object that has to be persisted.
	 * @throws DAOTrackException 	If persisting the track fails,
	 * 								specific feedback is given.
	 */
	public void persist(Track track) throws DAOTrackException;
	
	/**
	 * Deletes a track from the database, if there are no images assigned to it. 
	 * If images are assigned to the track, the delete flag is set.
	 * @param trackId Id of track to be deleted.
	 * @throws DAOTrackException If deletion fails, specific feedback is given.
	 */
	public void delete(int trackId) throws DAOTrackException;
	
	/**
	 * Returns all non-deleted tracks persisted in the database.
	 * @return See description.
	 * @throws DAOTrackException If query fails, specific feedback is given.
	 */
	public ArrayList<Track> findAll() throws DAOTrackException;
	
}
