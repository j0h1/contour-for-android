//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.server.dao;

import java.util.ArrayList;

import com.j0h1.contour.admin.shared.DAOImageException;
import com.j0h1.contour.admin.shared.domain.Image;

/**
 * Defines database access methods concerning Image objects.
 * @author Johannes Riedmann
 *
 */
public interface DAOImage {
	
	/**
	 * Persists an image (original image + edge image) with translations 
	 * to the database.
	 * @param titleEN			English title of the image.
	 * @param titleDE			German title of the image.
	 * @param originalContent	Base64 encoded content of the original image.
	 * @param edgeContent		Base64 encoded content of the edge image.
	 */
	public void persist(String titleEN, String titleDE, String originalContent, 
			String edgeContent) throws DAOImageException;
	
	/**
	 * Deletes an image from database if it's currently not in a track.
	 * Sets deleted flag of image to true if image is currently in a track.
	 * @param imageId ID of image to be deleted.
	 */
	public void delete(int imageId) throws DAOImageException;
	
	/**
	 * Returns List of non-deleted images.
	 * @return see description
	 */
	public ArrayList<Image> findAll() throws DAOImageException;

}
