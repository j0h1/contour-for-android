//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.server.dao;

import java.sql.SQLException;

import com.j0h1.contour.admin.server.utils.DatabaseManager;
import com.j0h1.contour.admin.shared.DAOException;

/**
 * Encapsulates methods that are used by all DAO implementations.
 * @author Johannes Riedmann
 *
 */
public class AbstractDAO {
	
	/**
	 * Opens the connection to the database.
	 */
	public void openConnection() {
		try {
			DatabaseManager.getInstance().openConnection();
		} catch (ClassNotFoundException ex) {
			throw new DAOException("Unable to connect to database. "
					+ "HSQLDB-JDBC-driver couldn't be found.");
		}
	}
	
	/**
	 * Closes the connection to the database.
	 */
	public void closeConnection() {
		try {
			DatabaseManager.getInstance().closeConnection();
		} catch (SQLException e) {
			throw new DAOException("Unable to close database connection.");
		}
	}
	
	/**
	 * Enables auto-commit if parameter autoCommit is true, disables it otherwise.
	 * @param autoCommit To disable/enable auto-commit for database.
	 */
	public void setAutoCommit(boolean autoCommit) {
		try {
			if (DatabaseManager.getInstance().getConnection() != null) {
				DatabaseManager.getInstance().getConnection().setAutoCommit(autoCommit);
			}			
		} catch (SQLException ex) {
			throw new DAOException("Unable to set auto commit.");
		}
	}

}
