//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.server.dao;

import java.util.ArrayList;

import com.j0h1.contour.admin.shared.DAOSessionException;
import com.j0h1.contour.admin.shared.domain.Session;

/**
 * Defines database access methods concerning Session objects.
 * @author Johannes Riedmann
 *
 */
public interface DAOSession {
	
	/**
	 * Retrieves a list of all sessions (custom and track sessions) persisted
	 * in the database.
	 * @return List of all sessions in the database.
	 * @throws DAOSessionException 	If the query fails, 
	 * 								specific feedback is returned
	 */
	public ArrayList<Session> findAll() throws DAOSessionException;
	
	/**
	 * Deletes a session with specified sessionId from the database. Along with
	 * the session, the drawings and score images are deleted. If the session is
	 * a custom session, the original data, as well as the edge data is deleted too.
	 * @param sessionId ID of the session to be deleted.
	 * @throws DAOSessionException 	If deletion fails, specific feedback
	 * 								is returned.
	 */
	public void delete(int sessionId) throws DAOSessionException;

}
