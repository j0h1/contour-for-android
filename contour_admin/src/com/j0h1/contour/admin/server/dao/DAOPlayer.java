//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.server.dao;

import java.util.ArrayList;

import com.j0h1.contour.admin.shared.DAOPlayerException;
import com.j0h1.contour.admin.shared.domain.Player;

/**
 * Defines database access methods concerning Player objects.
 * @author Johannes Riedmann
 *
 */
public interface DAOPlayer {
	
	/**
	 * Returns all persisted players.
	 * @return see description
	 * @throws DAOPlayerException 	If query fails, provides
	 * 								specific feedback.
	 */
	public ArrayList<Player> findAll() throws DAOPlayerException;
	
	/**
	 * Deletes the player with the specified playerId and all his
	 * drawing sessions/drawings (all data associated with this
	 * player).
	 * @param playerId ID of player to be deleted.
	 * @throws DAOPlayerException 	If deletion fails, provides
	 * 								specific feedback.
	 */
	public void delete(int playerId) throws DAOPlayerException;

}
