//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.server;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.j0h1.contour.admin.client.service.PlayerService;
import com.j0h1.contour.admin.server.dao.DAOPlayer;
import com.j0h1.contour.admin.server.dao.impl.DAOPlayerImpl;
import com.j0h1.contour.admin.shared.DAOPlayerException;
import com.j0h1.contour.admin.shared.domain.Player;

/**
 * RemoteServiceServlet that implements methods defined in PlayerService.
 * @author Johannes Riedmann
 *
 */
public class PlayerServiceImpl extends RemoteServiceServlet implements
		PlayerService {
	
	private static final long serialVersionUID = 1L;

	@Override
	public ArrayList<Player> findAll() throws DAOPlayerException {
		DAOPlayer playerDao = new DAOPlayerImpl();
		return playerDao.findAll();
	}

	@Override
	public void delete(int playerId) throws DAOPlayerException {
		DAOPlayer playerDao = new DAOPlayerImpl();
		playerDao.delete(playerId);
	}

}
