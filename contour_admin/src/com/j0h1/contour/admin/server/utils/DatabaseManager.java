//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.server.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Singleton class to handle HSQLDB database access.
 * @author Johannes Riedmann
 *
 */
public class DatabaseManager {

	private static DatabaseManager dbm;
	private Connection connection;
	
	protected DatabaseManager() {
		dbm = this;
	}
	
	/**
	 * Creates/returns existing instance of DatabaseManager.
	 * @return Instance of DatabaseManager.
	 */
	public static DatabaseManager getInstance() {
		if (dbm == null)
			return new DatabaseManager();
		return dbm;
	}
	
	/**
	 * Opens a connection to HSQLDB-Database.
	 * @throws ClassNotFoundException 	When JDBC driver for HSQLDB is 
	 * 									not installed properly.
	 */
	public void openConnection() throws ClassNotFoundException {
		// load the hsqldb-JDBC driver using the current class loader
	    Class.forName("org.hsqldb.jdbcDriver");
	    
	    try {
	    	connection = DriverManager.
	    			getConnection("jdbc:hsqldb:hsql://localhost/ContourDB", "SA", "");
		} catch (SQLException e) {
			// database server not started
		}
	}
	
	public Connection getConnection() {
		return connection;
	}
	
	/**
	 * Closes connection to database
	 * @throws SQLException If closing is not possible.
	 */
	public void closeConnection() throws SQLException {
		if (connection != null) {
			connection.close();
			connection = null;
		}		
	}
}
