//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.admin.server;

import java.util.ArrayList;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.j0h1.contour.admin.client.service.SessionService;
import com.j0h1.contour.admin.server.dao.DAOSession;
import com.j0h1.contour.admin.server.dao.impl.DAOSessionImpl;
import com.j0h1.contour.admin.shared.DAOSessionException;
import com.j0h1.contour.admin.shared.domain.Session;

/**
 * RemoteServiceServlet that implements methods defined in SessionService.
 * @author Johannes Riedmann
 *
 */
public class SessionServiceImpl extends RemoteServiceServlet implements
		SessionService {
	
	private static final long serialVersionUID = 1L;

	@Override
	public ArrayList<Session> findAll() throws DAOSessionException {
		DAOSession sessionDao = new DAOSessionImpl();
		return sessionDao.findAll();
	}

	@Override
	public void delete(int sessionId) throws DAOSessionException {
		DAOSession sessionDao = new DAOSessionImpl();
		sessionDao.delete(sessionId);
	}

}
