//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour;

import java.util.List;

import com.j0h1.contour.R;
import com.j0h1.contour.pojo.Session;
import com.j0h1.contour.pojo.Track;

import android.content.Context;
import android.graphics.Typeface;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

/**
 * Custom ExpandableListAdapter for the ExpandableListView that displays
 * the top10 track sessions for each track.
 * @author Johannes Riedmann
 *
 */
public class HighscoreExpandableListAdapter extends BaseExpandableListAdapter {
	
	private Context context;
    private List<Track> listDataHeader;
    // elements are stored with trackId as key and a list of sessions as value
    private SparseArray<List<Session>> listDataChild;
    
    public HighscoreExpandableListAdapter(Context context, List<Track> listDataHeader,
            SparseArray<List<Session>> listChildData) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listChildData;
    }

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// returns null if there are no values in listDataChild assigned to the
		// trackId, at the given groupPosition - otherwise it returns the list of
		// sessions assigned to the trackId
		if (listDataChild.get(getTrackIdByGroupPosition(groupPosition)) == null) {
			return null;
		}
		
		return listDataChild.get(getTrackIdByGroupPosition(groupPosition)).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		String childText = "";
		
		// when child is present, define String to show some information about it
		if (getChild(groupPosition, childPosition) != null) {
			Session child = (Session) getChild(groupPosition, childPosition);
			
			childText = "#" + (childPosition + 1) + 
					" " + child.getPlayer().getPlayername() + " " + child.getScore();
		}
		
		// inflate a layout to show childText, if convertView is null
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.highscore_list_item, null);
        }
 
        // access the inflated layout's TextView and set childText
        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.txtHighscoreItem);
 
        txtListChild.setText(childText);
        
        return convertView;
	}
	
	/**
	 * Since trackId is used as the key for the session lists and only indexes 
	 * are passed to the methods, the trackId to a given groupPosition has to be
	 * found.
	 * @param groupPosition	Index of group.
	 * @return	trackId of the Track object at the given groupPosition.
	 */
	private int getTrackIdByGroupPosition(int groupPosition) {
		return ((Track) getGroup(groupPosition)).getTrackId();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		if (listDataChild.get(getTrackIdByGroupPosition(groupPosition)) == null) {
			return 0;
		}
		return listDataChild.get(getTrackIdByGroupPosition(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return listDataHeader.get(groupPosition);
	}
	
	/**
	 * Reverse function to getTrackIdByGroupPosition.
	 * @param trackId	TrackId of Track to find groupPosition of.
	 * @return			GroupPosition of Track in the header list.
	 * 					Integer.MAX_VALUE if trackId was not found.
	 */
	public int getGroupPositionByTrackId(int trackId) {
		for (int i = 0; i < listDataHeader.size(); i++) {
			if (listDataHeader.get(i).getTrackId() == trackId) {
				return i;
			}
		}
		
		return Integer.MAX_VALUE;
	}

	@Override
	public int getGroupCount() {
		return listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		// define the header title (which is the track name)
		final String headerTitle = ((Track) getGroup(groupPosition)).getTrackname();
		
		// inflate layout for header title if convertView is null
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.highscore_list_group, null);
        }
 
        // set header title to TextView provided by the inflated layout
        TextView lblListHeader = (TextView) convertView
                .findViewById(R.id.txtHighscoreGroup);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
 
        return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
