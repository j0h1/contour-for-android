//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.utils;

/**
 * Configuration class for REST web service.
 * @author Johannes Riedmann
 *
 */
public class RESTConfig {
	
	// IP address that hosts the web service
	public static final String REST_SERVICE_IP = "83.212.126.229";
	
	// port of the web service (since it's a HTTP web service,
	// it should be "80" or "8080")
	public static final String REST_SERVICE_PORT = "8080";
	
	// full path to reach the web service's REST interface
	public static final String BASE_URL = 
			"http://" + REST_SERVICE_IP + ":" + REST_SERVICE_PORT + "/rest/";
	
}
