//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import com.j0h1.contour.R;
import com.j0h1.contour.database.ContourDatabase;
import com.j0h1.contour.pojo.Player;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity that is responsible for showing the user his final score and
 * giving the options to either save or discard the finished drawing session.
 * @author Johannes Riedmann
 *
 */
public class SessionFinishedActivity extends Activity {
	
	private Toast toast;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		setContentView(R.layout.activity_session_finished);
		
		// get the locally persisted player
		ContourDatabase db = new ContourDatabase(getApplicationContext());
		Player player = db.findPlayer();
		double score = db.getCurrentScore();
		db.close();
		
		// compliment the user for his performance
		TextView congratsUser = (TextView) findViewById(R.id.txtCongratsUser);
		congratsUser.setText(getString(R.string.congratulations_user) + ", " + player.getPlayername() + "!");
		
		// if the upload failed, an extra String with the key "error" is passed to the Activity
		Intent intent = getIntent();
		String error = intent.getStringExtra("error");
		
		if (error != null) {
			prepareToast(getString(R.string.uploading_data_failed));
			toast.show();
		}

		// show score in scoreView
		TextView scoreView = (TextView) findViewById(R.id.txtScore);
		// format the score
		DecimalFormat scoreFormat = new DecimalFormat("#.##");
		scoreFormat.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.US));
		scoreView.setText(Double.valueOf(scoreFormat.format(score)) + ".");
		
		// when the save button is clicked, start the UploadLoadingScreenActivity
		ImageButton saveButton = (ImageButton) findViewById(R.id.imbSaveSession);
		saveButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(SessionFinishedActivity.this, 
						UploadLoadingScreenActivity.class);
    			startActivity(intent);
			}
		});
		
		// when the discard button is clicked, delete all local data (except for player data)
		// and redirect the user to the MainMenuActivity
		ImageButton discardButton = (ImageButton) findViewById(R.id.imbDiscardSession);
		discardButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				ContourDatabase db = new ContourDatabase(getApplicationContext());
				db.discardSession();
				db.close();
	
				Intent intent = new Intent(SessionFinishedActivity.this, 
						MainMenuActivity.class);
    			startActivity(intent);
			}			
		}); 
	}
	
	@Override
	public void onBackPressed() {
		// do nothing to avoid unwanted behavior (jumping back into the GameActivity)
	}
	
	@SuppressLint("ShowToast") 
	private void prepareToast(String message) {
		if (toast != null) {
    		toast.cancel();
    	}
    	toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
	}

}
