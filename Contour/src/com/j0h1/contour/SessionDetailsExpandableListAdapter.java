//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour;

import java.util.ArrayList;

import com.j0h1.contour.R;
import com.j0h1.contour.pojo.ImagePojo;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

/**
 * ExpandableListAdapter for ExpandableListView used to display 
 * the different types of image data of a custom or track session.
 * @author Johannes Riedmann
 *
 */
public class SessionDetailsExpandableListAdapter extends
		BaseExpandableListAdapter {
	
	private Context context;
	private ArrayList<ImagePojo> listDataHeader;
	private String[] listDataChild;
	
	public SessionDetailsExpandableListAdapter(Context context, ArrayList<ImagePojo> listDataHeader,
			String[] listDataChild) {
        this.context = context;
        this.listDataHeader = listDataHeader;
        this.listDataChild = listDataChild;
    }

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return listDataChild[childPosition];
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		String childText = (String) getChild(groupPosition, childPosition);
		
		if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.session_details_image_child, null);
        }
		
		TextView txtListChild = (TextView) convertView
                .findViewById(R.id.txtSessionDetailsChild);
		
		txtListChild.setText(childText);
        
        return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return 4;
	}

	@Override
	public Object getGroup(int groupPosition) {
		if (listDataHeader != null) {
			return listDataHeader.get(groupPosition);
		}
		return null;
	}

	@Override
	public int getGroupCount() {
		if (listDataHeader != null) {
			return listDataHeader.size();
		}
		return 0;
	}

	@Override
	public long getGroupId(int groupPosition) {
		if (listDataHeader != null) {
			return listDataHeader.get(groupPosition).getImageId();
		}
		return 0;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		final String imagename = listDataHeader.get(groupPosition).getImagename();
		final String score = "" + listDataHeader.get(groupPosition).getScore();
		
		if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.session_details_image_group, null);
        }
		
		TextView txtImageTitle = (TextView) convertView
                .findViewById(R.id.txtSessionDetailsImageName);
		txtImageTitle.setTypeface(null, Typeface.BOLD);
		txtImageTitle.setText(imagename);
		
		TextView txtImageScore = (TextView) convertView
                .findViewById(R.id.txtSessionDetailsScore);
		txtImageScore.setTypeface(null, Typeface.BOLD);
		txtImageScore.setText(" (" + score + ")");
        
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

}
