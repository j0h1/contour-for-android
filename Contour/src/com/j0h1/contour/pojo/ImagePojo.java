//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.pojo;

import java.io.Serializable;

import org.simpleframework.xml.Element;

/**
 * Serializable Image object representation that contains an 
 * images meta data as well as the binary data. This image
 * representation is needed to store data received from REST
 * calls.
 * @author Johannes Riedmann
 *
 */
public class ImagePojo implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Element(name = "imageId", required = false)
	private int imageId;
	@Element(name = "imagename", required = false)
	private String imagename;
	@Element(name = "position", required = false)
	private int position;
	@Element(name = "score", required = false)
	private double score;
	@Element(name = "data", required = false)
	private String data;
	
	public ImagePojo() {
		
	}
	
	public int getImageId() {
		return imageId;
	}
	public void setImageId(int imageId) {
		this.imageId = imageId;
	}
	public String getImagename() {
		return imagename;
	}
	public void setImagename(String imagename) {
		this.imagename = imagename;
	}
	public int getPosition() {
		return position;
	}
	public void setPosition(int position) {
		this.position = position;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}

}
