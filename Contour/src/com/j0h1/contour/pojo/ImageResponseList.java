//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.pojo;

import java.util.ArrayList;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

/**
 * List of serializable Image objects. 
 * @author Johannes Riedmann
 *
 */
@Root(name = "responseList")
public class ImageResponseList {
	
	@ElementList(entry = "list", inline = true, required = false)
	private ArrayList<ImagePojo> list;

	public ArrayList<ImagePojo> getList() {
		return list;
	}

	public void setList(ArrayList<ImagePojo> list) {
		this.list = list;
	}

}
