//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.pojo;

import java.io.Serializable;

import org.simpleframework.xml.Element;

/**
 * Serializable representation of a Session object, that can be mapped
 * to a XML document. It contains a sessions meta data as well as a
 * Track/Player object associated with the session.
 * @author Johannes Riedmann
 *
 */
public class Session implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Element(name = "sessionId")
	private int sessionId;
	@Element(name = "track")
	private Track track;
	@Element(name = "player")
	private Player player;
	@Element(name = "score")
	private double score;
	@Element(name = "date")
	private long date;
	
	public Session() {
		
	}
	
	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	
	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}
	
	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
}

