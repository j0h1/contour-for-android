//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.pojo;

import java.io.Serializable;
import java.util.ArrayList;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

/**
 * Serializable representation of a Track object, that can be mapped
 * to a XML document. It can contain a tracks meta data as well as a
 * list of images assigned to the track.
 * @author Johannes Riedmann
 *
 */
@Root
public class Track implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Element(name = "trackId", required = false)
	private int trackId;
	@Element(name = "trackname", required = false)
	private String trackname;
	@Element(name = "images", required = false)
	private ArrayList<ImagePojo> images;

	public Track() {
		
	}

	public int getTrackId() {
		return trackId;
	}

	public void setTrackId(int trackId) {
		this.trackId = trackId;
	}
	
	public String getTrackname() {
		return trackname;
	}

	public void setTrackname(String trackname) {
		this.trackname = trackname;
	}

	public ArrayList<ImagePojo> getImages() {
		return images;
	}

	public void setImages(ArrayList<ImagePojo> images) {
		this.images = images;
	}
	
}
