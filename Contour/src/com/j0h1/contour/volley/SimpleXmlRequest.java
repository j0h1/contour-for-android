//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.volley;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;

/**
 * Simple Volley class for doing XML HTTP requests which are parsed into Java
 * objects by the Simple framework.
 * @author Johannes Riedmann
 *
 */
public class SimpleXmlRequest<T> extends Request<T> {

	private static final Serializer serializer = new Persister();
	private final Class<T> clazz;
	private final Map<String, String> headers;
	private final Listener<T> listener;

	/**
	 * Make HTTP request and return a parsed object from Response Invokes the
	 * other constructor.
	 */
	public SimpleXmlRequest(int method, String url, Class<T> clazz,
			Listener<T> listener, ErrorListener errorListener) {
		this(method, url, clazz, null, listener, errorListener);
	}

	/**
	 * Make HTTP request and return a parsed object from XML response.
	 * @param url 			URL of the request to make.
	 * @param clazz 		Relevant class object.
	 * @param headers 		Map of request headers.
	 * @param listener		Listener for successful response.
	 * @param errorListener	Request returns with an Volley error.
	 * 
	 */
	public SimpleXmlRequest(int method, String url, Class<T> clazz,
			Map<String, String> headers, Listener<T> listener,
			ErrorListener errorListener) {
		super(method, url, errorListener);
		this.clazz = clazz;
		this.headers = headers;
		this.listener = listener;
	}

	@Override
	public Map<String, String> getHeaders() throws AuthFailureError {
		return headers != null ? headers : super.getHeaders();
	}

	@Override
	protected void deliverResponse(T response) {
		listener.onResponse(response);
	}

	@Override
	protected Response<T> parseNetworkResponse(NetworkResponse response) {
		try {
			String data = new String(response.data,
					HttpHeaderParser.parseCharset(response.headers));
			return Response.success(serializer.read(clazz, data),
					HttpHeaderParser.parseCacheHeaders(response));
		} catch (UnsupportedEncodingException e) {
			return Response.error(new ParseError(e));
		} catch (Exception e) {
			return Response.error(new VolleyError(e.getMessage()));
		}
	}
}