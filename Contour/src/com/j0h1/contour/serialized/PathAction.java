//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.serialized;

/**
 * Defines the actions a serialized Path object (CustomPath) can
 * perform.
 * @author Johannes Riedmann
 *
 */
public interface PathAction {

	/**
	 * Defines the actions that can be performed/stored by the 
	 * serialized path object.
	 */
	public enum PathActionType {
		LINE_TO, MOVE_TO, QUAD_TO, TRANSFORM;
	};

	public PathActionType getType();

}
