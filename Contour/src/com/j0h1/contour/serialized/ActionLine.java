//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.serialized;

import java.io.Serializable;

/**
 * Serialized representation of a line action, that can
 * be assigned to a Path.
 * @author Johannes Riedmann
 *
 */
public class ActionLine implements PathAction, Serializable {

	private static final long serialVersionUID = -1035421256278868702L;

	private float x, y;

	public ActionLine(float x, float y) {
		this.x = x;
		this.y = y;
	}

	@Override
	public PathActionType getType() {
		return PathActionType.LINE_TO;
	}
	
	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}

}
