//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.serialized;

import java.io.Serializable;

/**
 * Serialized representation of a quadTo action, that can
 * be assigned to a Path.
 * @author Johannes Riedmann
 *
 */
public class ActionQuad implements PathAction, Serializable {
	
	private static final long serialVersionUID = -1056322449414356525L;
	
	private float x1, y1, x2, y2;
	
	public ActionQuad(float x1, float y1, float x2, float y2) {
		this.x1 = x1;
		this.y1 = y1;
		this.x2 = x2; 
		this.y2 = y2;
	}

	@Override
	public PathActionType getType() {
		return PathActionType.QUAD_TO;
	}
	
	public float getX1() {
		return x1;
	}
	
	public float getY1() {
		return y1;
	}
	
	public float getX2() {
		return x2;
	}
	
	public float getY2() {
		return y2;
	}

}
