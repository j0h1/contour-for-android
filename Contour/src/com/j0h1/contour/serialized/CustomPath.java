//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.serialized;

import java.io.Serializable;
import java.util.ArrayList;

import com.j0h1.contour.serialized.PathAction.PathActionType;

import android.graphics.Path;

/**
 * Serializable Path object that can be stored in the local database.
 * This representation holds a list of PathActions that define the path. 
 * The path bounds, scale factor and the flag for portrait path
 * are used to restore the path successfully to the screen after retrieving 
 * the path data from the local database.
 * @author Johannes Riedmann
 *
 */
public class CustomPath extends Path implements Serializable {

	private static final long serialVersionUID = 491472636386719277L;

	private ArrayList<PathAction> actions = new ArrayList<PathAction>();
	
	private float[] pathBounds;
	private float scaleFactor;	
	private boolean isPortraitPath;
	
	public CustomPath() {
		
	}

	@Override
	public void moveTo(float x, float y) {
		actions.add(new ActionMove(x, y));
		super.moveTo(x, y);
	}

	@Override
	public void lineTo(float x, float y) {
		actions.add(new ActionLine(x, y));
		super.lineTo(x, y);
	}
	
	@Override
	public void quadTo(float x1, float y1, float x2, float y2) {
		actions.add(new ActionQuad(x1, y1, x2, y2));
		super.quadTo(x1, y1, x2, y2);
	}
	
	public void transform(CustomMatrix transformationMatrix) {		
		actions.add(new ActionTransform(transformationMatrix));
		super.transform(transformationMatrix);
	}
	
	public ArrayList<PathAction> getActions() {
		return actions;
	}

	/**
	 * This method is used to draw the path from it's associated list of actions.
	 */
	public void drawThisPath() {
		for (PathAction p : actions) {
			if (p.getType().equals(PathActionType.MOVE_TO)) {
				super.moveTo(((ActionMove) p).getX(), ((ActionMove) p).getY());
			} else if (p.getType().equals(PathActionType.LINE_TO)) {
				super.lineTo(((ActionLine) p).getX(), ((ActionLine) p).getY());
			} else if (p.getType().equals(PathActionType.QUAD_TO)) {
				super.quadTo(((ActionQuad) p).getX1(), ((ActionQuad) p).getY1(),
					((ActionQuad) p).getX2(), ((ActionQuad) p).getY2());
			} else if (p.getType().equals(PathActionType.TRANSFORM)) {
				super.transform(((ActionTransform) p).getTransformationMatrix());
			}
		}
	}
	
	public float[] getPathBounds() {
		return pathBounds;
	}
	
	public void setPathBounds(float[] pathBounds) {
		this.pathBounds = pathBounds;
	}

	public float getScaleFactor() {
		return scaleFactor;
	}

	public void setScaleFactor(float scaleFactor) {
		this.scaleFactor = scaleFactor;
	}

	public boolean isPortraitPath() {
		return isPortraitPath;
	}

	public void setPortraitPath(boolean isPortraitPath) {
		this.isPortraitPath = isPortraitPath;
	}

}
