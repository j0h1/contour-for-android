//	Contour - Android drawing teacher application
//	Copyright (C) 2014  Johannes Riedmann
//	
//	This program is free software: you can redistribute it and/or modify
//	it under the terms of the GNU General Public License as published by
//	the Free Software Foundation, either version 3 of the License, or
//	(at your option) any later version.
//	
//	This program is distributed in the hope that it will be useful,
//	but WITHOUT ANY WARRANTY; without even the implied warranty of
//	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//	GNU General Public License for more details.
//	
//	You should have received a copy of the GNU General Public License
//	along with this program.  If not, see <http://www.gnu.org/licenses/>.

package com.j0h1.contour.serialized;

import java.io.Serializable;

import android.graphics.Matrix;

/**
 * The CustomPath's transformation method requires a serialized
 * representation of a Matrix object.
 * @author Johannes Riedmann
 *
 */
public class CustomMatrix extends Matrix implements Serializable {
	
	private static final long serialVersionUID = -1439940802626478060L;
	
	public CustomMatrix() {
		
	}

}
